import multiprocessing
import time

def worker():
    name = multiprocessing.current_process().name
    print name, 'Starting'
    time.sleep(10)
    print name, 'Exiting'

def my_service():
    name = multiprocessing.current_process().name
    print name, 'Starting'
    time.sleep(2)
    print name, 'Exiting'

def run_process():
    service = multiprocessing.Process(name='my_service', target=my_service)
    service.daemon = False
    worker_1 = multiprocessing.Process(name='worker 1', target=worker)
    worker_1.daemon = True
    worker_1.start()
    service.start()
    #worker_1.terminate()

if __name__ == '__main__':
    proc = multiprocessing.Process(name='run_process', target=run_process)
    proc.start()
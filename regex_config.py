import re

NO_RAKE_TABLES = ['No Rake Micro Stakes']

RGX_CONF_SUB = { 
	'SEAT'		: r'(?:Seat\s(?P<seat>\d):)',
	'PLYR'		: r'(?P<player>[a-zA-Z0-9_-]+)',
	'CHIPS'		: r'(?P<chips>[.0-9]+)',
	'HNUM'		: r'(?P<hand_num>\d+\D\d+)',
	'HTIME'		: r'(?P<hand_time>\d{2}:\d{2}:\d{2})',
	'HDATE'		: r'(?P<hand_date>\d{4}\D\d{2}\D\d{2})',
	'LIMIT'		: r'(?P<limit>PL|NL|L)', #'NLHE 6max .25/.5 #1',
	'GAMETYPE'	: r'(?P<game>HE|O8|O)',
	'SEATTYPE'	: r'(?P<seat_type>HU|6max|9max)',
	'BUYIN'		: r'\((?P<min_buyin>[.0-9]+)\s\-\s(?P<max_buyin>[.0-9]+)\)',
	'CARDS'		: r'(?:\[(?P<cards>[AJKQTchds 0-9]+)\])',
	'ACTS'		: r'(?P<action>folds|checks|calls|bets|raises)',
	
	'game'		: r'Game\:',
	'table'		: r'Table\:',
	'hand'		: r'Hand \#',
	'rake'		: r'Rake \(',
	'winners'	: r'(?:wins|splits)(?:\s\w+)* Pot(?:\s\w+)*',
	
	'preflop'	: r'\*\* Hole Cards \*\*',
	'flop'		: r'\*\* Flop \*\*',
	'turn'		: r'\*\* Turn \*\*',
	'river'		: r'\*\* River \*\*',
	'SD'		: r'\*\*[ \w+]* Pot[ \w+]* Show Down \*\*', 
	'allin'		: r'All\-in',

	'sb'		: r'posts small blind',
	'bb'		: r'posts big blind',
	'sbbb'		: r'posts small \& big blind',
	'dealer'	: r'has the dealer button',

	'all bet'	: r'(?P<seat>\d{1})(?:[RCFXB][0-9]+(?:[.]\d{2}))',
	'no bet'	: r'(?:\d{1}[CFX][0-9]+(?:[.]\d{2}))',
	'no check'	: r'(?:\d{1}[CFR][0-9]+(?:[.]\d{2}))',
	'bet'		: r'(?:\d{1}[B][0-9]+(?:[.]\d{2}))',
	'raise'	  	: r'(?:\d{1}[R][0-9]+(?:[.]\d{2}))',
	'fold'		: r'(?:\d{1}[F][0-9]+(?:[.]\d{2}))',
	'call'		: r'(?:\d{1}[C][0-9]+(?:[.]\d{2}))',

	'vpip' 		: r'(?P<seat>\d{1})(?:R|C)(?:[0-9]+(?:[.]\d{2}))',
	'op vpip' 	: r'(?:(?P<seat>\d{1})[CFR][0-9]+(?:[.]\d{2}))',
	'pfr' 		: r'^(?:%(no bet)s*(?:(?P<seat>\d{1})R[0-9]+(?:[.]\d{2})))',
	
	'pf3b' 		: r'^(?:%(no bet)s*%(raise)s%(no bet)s*(?:(?P<seat>\d{1})R[0-9]+(?:[.]\d{2})))',
	'f pf3b' 	: r'^(?:%(no bet)s*%(raise)s%(no bet)s*%(raise)s%(call)s*(?:(?P<seat>\d{1})[F][0-9]+(?:[.]\d{2})))',
	'c pf3b' 	: r'^(?:%(no bet)s*%(raise)s%(no bet)s*%(raise)s%(fold)s*(?:(?P<seat>\d{1})[C][0-9]+(?:[.]\d{2})))',
	'r pf3b' 	: r'^(?:%(no bet)s*%(raise)s%(no bet)s*%(raise)s(?:\d{1}[CF][0-9]+(?:[.]\d{2}))*(?:(?P<seat>\d{1})[R][0-9]+(?:[.]\d{2})))',

	'initiative': r'(?:%(no bet)s*(?:(?P<seat>\d{1})[RB][0-9]+(?:[.]\d{2}))%(no bet)s*)$',

	'cb pfx'	: r'(?:%s[B][0-9]+(?:[.]\d{2}))',
	'cbet'		: r'(?:(?P<seat>%s)[B][0-9]+(?:[.]\d{2}))',
	'op cb'		: r'(?:(?P<seat>%s)[XB][0-9]+(?:[.]\d{2}))',
	'f cb'		: r'(?:%s(?:\d{1}[C][0-9]+(?:[.]\d{2}))*(?:(?P<seat>\d{1})[F][0-9]+(?:[.]\d{2})))',
	'c cb'		: r'(?:%s(?:\d{1}[F][0-9]+(?:[.]\d{2}))*(?:(?P<seat>\d{1})[C][0-9]+(?:[.]\d{2})))',
	'r cb'		: r'(?:%s(?:\d{1}[CF][0-9]+(?:[.]\d{2}))*(?:(?P<seat>\d{1})[R][0-9]+(?:[.]\d{2})))'
	}

RGX_CONF_info 		= re.compile(r'^%(hand)s%(HNUM)s - %(HDATE)s %(HTIME)s$' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_buyin		= re.compile(r'^%(game)s\s(?:[\s\w+\'\-])+\s%(BUYIN)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_table		= re.compile(r'^%(table)s %(LIMIT)s%(GAMETYPE)s %(SEATTYPE)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_players 	= re.compile(r'^%(SEAT)s %(PLYR)s \(%(CHIPS)s\)' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_date 		= re.compile(r'%(HDATE)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_hand		= re.compile(r'^%(hand)s%(HNUM)s{1}' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_rake		= re.compile(r'^%(rake)s%(CHIPS)s\){1}' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_winner 	= re.compile(r'^%(PLYR)s %(winners)s \(%(CHIPS)s\)' % RGX_CONF_SUB, re.MULTILINE)

RGX_CONF_sb 		= re.compile(r'^%(PLYR)s %(sb)s %(CHIPS)s' % RGX_CONF_SUB, re.MULTILINE ) 
RGX_CONF_bb 		= re.compile(r'^%(PLYR)s %(bb)s %(CHIPS)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_sbbb 		= re.compile(r'^%(PLYR)s %(sbbb)s %(CHIPS)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_dealer 	= re.compile(r'^%(PLYR)s %(dealer)s$' % RGX_CONF_SUB, re.MULTILINE)

RGX_CONF_action 	= re.compile(r'^%(PLYR)s %(ACTS)s(?:[a-z A-Z])*%(CHIPS)s?(?:\s\((%(allin)s)\))?' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_refund		= re.compile(r'^%(PLYR)s refunded %(CHIPS)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_shown 		= re.compile(r'^%(PLYR)s shows %(CARDS)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_hero		= re.compile(r'^Dealt to %(PLYR)s %(CARDS)s' % RGX_CONF_SUB, re.MULTILINE)

RGX_CONF_preflop	= re.compile(r'^(?:%(preflop)s)' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_flop		= re.compile(r'^(?:%(flop)s) %(CARDS)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_turn		= re.compile(r'^(?:%(turn)s) %(CARDS)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_river		= re.compile(r'^(?:%(river)s) %(CARDS)s' % RGX_CONF_SUB, re.MULTILINE)
RGX_CONF_sd 		= re.compile(r'^(?:%(SD)s) %(CARDS)s' % RGX_CONF_SUB, re.MULTILINE)

RGX_CONF_all_bet	= re.compile(r'%(all bet)s' % RGX_CONF_SUB)
RGX_CONF_raise 		= re.compile(r'%(raise)s' % RGX_CONF_SUB)
RGX_CONF_vpip 		= re.compile(r'%(vpip)s' % RGX_CONF_SUB)
RGX_CONF_op_vpip	= re.compile(r'%(op vpip)s' % RGX_CONF_SUB)
RGX_CONF_pfr 		= re.compile(r'%(pfr)s' % RGX_CONF_SUB % RGX_CONF_SUB)
RGX_CONF_pf3b 		= re.compile(r'%(pf3b)s' % RGX_CONF_SUB % RGX_CONF_SUB)
RGX_CONF_f_pf3b 	= re.compile(r'%(f pf3b)s' % RGX_CONF_SUB % RGX_CONF_SUB)
RGX_CONF_c_pf3b 	= re.compile(r'%(c pf3b)s' % RGX_CONF_SUB % RGX_CONF_SUB)
RGX_CONF_r_pf3b 	= re.compile(r'%(r pf3b)s' % RGX_CONF_SUB % RGX_CONF_SUB)
RGX_CONF_initiative = re.compile(r'%(initiative)s' % RGX_CONF_SUB % RGX_CONF_SUB)

import Queue
import threading

def basic_worker(queue):
    while True:
        item = queue.get()
        # do_work(item)
        print(item)
        print "yolo"
        queue.task_done()
def basic():
    # http://docs.python.org/library/queue.html
    queue = Queue.Queue()
    for i in range(3):
         t = threading.Thread(target=basic_worker,args=(queue,))
         #t.daemon = True
         t.start()
    for item in range(4):
        queue.put(item)
        print "nolo"
    queue.join()       # block until all tasks are done
    print('got here')

basic()
print "got there"


import Queue
import threading
import urllib2

# called by each thread
def get_url(q, url):
    q.put(urllib2.urlopen(url).read())

theurls = '''http://google.com http://yahoo.com'''.split()

q = Queue.Queue()

for u in theurls:
    t = threading.Thread(target=get_url, args = (q,u))
    #t.daemon = True
    t.start()

s = q.get()
print s

import multiprocessing

def worker(n):
    """worker function"""
    print 'Worker' + str(n)
    return

def run():
    jobs = []
    for i in range(5):
        p = multiprocessing.Process(target=worker, args = (i,))
        jobs.append(p)
        p.start()


# This may be incredibly useful

import multiprocessing

class MyFancyClass(object):
    
    def __init__(self, name):
        self.name = name
    
    def do_something(self):
        proc_name = multiprocessing.current_process().name
        print 'Doing something fancy in %s for %s!' % (proc_name, self.name)


def worker(q):
    obj = q.get()
    obj.do_something()


if __name__ == '__main__':
    queue = multiprocessing.Queue()

    p = multiprocessing.Process(target=worker, args=(queue,))
    p.start()
    
    queue.put(MyFancyClass('Fancy Dan'))
    
    # Wait for the worker to finish
    queue.close()
    queue.join_thread()
    p.join()


    # Used to buffer responses from the server. This is used because sometimes server responses are longer than a single packet
    def __processBuffer(self, bufferObj):


        if (bufferObj):
            pos = bufferObj.find(END_STR)
            if (pos != -1 ):
                self.nextBlock = bufferObj[:pos]
                bufferObj = bufferObj[pos+1:]
                return True
            else:
                bufferObj = bufferObj + self.__recv()
                return False
        else:
            self.nextBlock = ''
            return None

    # Process input
    def __processInput(self, bufferObj):
        result = self.__processBuffer()
        if (result == None ): 
            return None
        elif (result != False):
            if (self.nextBlock.find('Command') != -1):
                responseArr = urlparse.parse_qs(self.nextBlock)
                try: 
                    responseArr['Command']
                    command = responseArr['Command']
                    if ('Session' in [command, command[0]]): 
                        self.__ID = responseArr['ID'] #?
                    else:
                        return responseArr
                except NameError:
                    pass
        else:
            return True
            



    # Our main loop which is called continuously after connection
    def stream(self, rawstream = False):
        if (not self.__socket):
            return False #change to sys.exits later
        else:
            if (rawstream == True):
                ret = self.getPackets(True)
            else:
                ret = self.getPackets(False)
        return ret


    regex_info = re.compile(r'^(?:Hand #)(?P<hand_num>\d+\D\d+)\s\D\s(?P<hand_date>\d{4}\D\d{2}\D\d{2})\s(?P<hand_time>\d{2}:\d{2}:\d{2})', re.MULTILINE)
    
    regex_sb = re.compile(r'(?:(?P<sb>[a-zA-Z0-9-_]+) posts small blind+)|(?:(?P<bb>[a-zA-Z0-9-_]+) posts big blind+)')
    regex_bb = re.compile(r'((?P<bb>[a-zA-Z0-9-_]+) posts big blind+)')
    re_PostBoth = re.compile(r"^%(PLYR)s posts small \& big blind (?P<SBBB>[.0-9]+)" %  substitutions, re.MULTILINE)

re_sub = { 
	'PLYR'		: r'(?P<players>[a-zA-Z0-9_-]+)',
	'CHIPS'		: r'(?P<chips>[.0-9]+)',
	'HNUM'		: r'(?P<hand_num>\d+\D\d+)',
	'HTIME'		: r'(?P<hand_time>\d{2}:\d{2}:\d{2})',
	'HDATE'		: r'(?P<hand_date>\d{4}\D\d{2}\D\d{2})',
	'CARDS'		: r'(?:\[(?P<cards>[AJKQTchds 0-9]+)\])',
	'ACTS'		: r'(?P<actions>folds|checks|calls|bets|raises)',
	'hand'		: r'Hand \#',
	'rake'		: r'Rake \(',
	'preflop'	: r'\*\* Hole Cards \*\*',
	'flop'		: r'\*\* Flop \*\*',
	'turn'		: r'\*\* Turn \*\*',
	'river'		: r'\*\* River \*\*',
	'SD'		: r'\*\*[ \w+]* Pot[ \w+]* Show Down \*\*', 
	'allin'		: r'All\-in',
	'sb'		: r'posts small blind',
	'bb'		: r'posts big blind',
	'sbbb'		: r'posts small \& big blind',
	'dealer'	: r'has the dealer button'
	}


def parseHand(hand):

    [m.groupdict() for m in regex_players.finditer(hand)] 


regex_info 		= re.compile(r'^%(hand)s%(HNUM)s - %(HDATE)s %(HTIME)s$' % re_sub, re.MULTILINE)
regex_players 	= re.compile(r'^(?:Seat\s\d:)\s(?P<player>[a-zA-Z0-9_-]+)\s\((?P<chips>\d+\D?\d+)\)', re.MULTILINE)
regex_sb 		= re.compile(r'^%(PLYR)s %(sb)s %(CHIPS)s' % re_sub, re.MULTILINE ) 
regex_bb 		= re.compile(r'^%(PLYR)s %(bb)s %(CHIPS)s' % re_sub, re.MULTILINE)
regex_sbbb 		= re.compile(r'^%(PLYR)s %(sbbb)s %(CHIPS)s' % re_sub, re.MULTILINE)
regex_dealer 	= re.compile(r'^%(PLYR)s %(dealer)s$' % re_sub, re.MULTILINE)
regex_action 	= re.compile(r'^%(PLYR)s %(ACTS)s(?:[a-z A-Z])*%(CHIPS)s?(?: \((%(allin)s)\))?' % re_sub, re.MULTILINE)
regex_winner 	= re.compile(r'^%(PLYR)s (?:wins|splits)(?: \w+)* Pot(?: \w+)* \(%(CHIPS)s\)' % re_sub, re.MULTILINE)
regex_shown 	= re.compile(r'^%(PLYR)s shows %(CARDS)s' % re_sub, re.MULTILINE)
regex_hero		= re.compile(r'^Dealt to %(PLYR)s %(CARDS)s' % re_sub, re.MULTILINE)
regex_flop		= re.compile(r'^(?:%(flop)s) %(CARDS)s' % re_sub, re.MULTILINE)
regex_turn		= re.compile(r'^(?:%(turn)s) %(CARDS)s' % re_sub, re.MULTILINE)
regex_river		= re.compile(r'^(?:%(river)s) %(CARDS)s' % re_sub, re.MULTILINE)
regex_sd 		= re.compile(r'^(?:%(SD)s) %(CARDS)s' % re_sub, re.MULTILINE)
regex_refund	= re.compile(r'^%(PLYR)s refunded %(CHIPS)s' % re_sub, re.MULTILINE)

def search(obj, name):
	import time
	num_calls = 0
	message = []
	timeout = time.time() + 0.1*60   #  minutes from now
	obj.sendSearch(name)
	while True:
		try:
			r = obj.stream(False)
			num_calls +=1
			if (not r): 
				break
			if (time.time() > timeout):
				obj.closeConnection()
			else: 
				time.sleep(0.1)
			if (r != True):
				message.append(r)
		except:
			break
	message = ["Number of calls =%s" % num_calls] + message
	return message


	'h_id' 	:	hand_attr['hand number'],
								'player':	player_map[k],
								'vpip'	:  	(1 if k in stats['vpip'] else 0),
								'pfr'	:  	(1 if k in stats['pfr'] else 0),
								'pf3b'	:  	(1 if k in stats['pf3b'] else 0),
								'f pf3b':  	(1 if k in stats['f pf3b'] else 0),
								'c pf3b':  	(1 if k in stats['c pf3b'] else 0),
								'r pf3b':  	(1 if k in stats['r pf3b'] else 0),
								'cbet'	:  	(1 if k in stats['cbet'] else 0),
								'op cb' :  	(1 if k in stats['op cb'] else 0),
								'f cb'	:  	(1 if k in stats['f cb'] else 0),
								'c cb'	:  	(1 if k in stats['c cb'] else 0),
								'r cb'	:	(1 if k in stats['r cb'] else 0)

 Hand_ID, Player_Name, vpip, pfr, pf3b, f_pf3b, c_pf3b, r_pf3b, cbet,	op_cbet, f_cbet, c_cbet, r_cbet

SELECT dt, SUM( amount_won), count(*) FROM (	SELECT * from hand_info AS HI INNER JOIN player_winnings AS PW ON (HI.hand_id = PW.hand_id) ) AS T WHERE UPPER(player_name) = UPPER('gerbera') GROUP BY player_name, dt limit 5;
 

'''

#preflop = attr['preflop']
#flop = attr['flop']


RGX_CONF_pfr 		= re.compile(r'%(pfr)s' % RGX_CONF_SUB % RGX_CONF_SUB)





import imp
imp.load_source('regex_config', '/home/fides/Downloads/Projects/swc-api/regex_config.py')
P = imp.load_source('SWC_PARSE', '/home/fides/Downloads/Projects/swc-api/swc_parse.py')
hnum = 15
S = P.SWC_PARSE()
idx = S.parseHH(hh)
hand = hh[ idx[hnum][0]: idx[hnum][1] ]
d = S.parseHand(hand)

def getHHList(hh_dir, path_to_idx = None):
	file_list = []
	if path_to_idx == None:
		idx_str = ''
	else:
		with open(path_to_idx, 'r') as idx:
			idx_str = idx.read()
			#mm = mmap.mmap( idx.fileno(), 0, access = mmap.ACCESS_READ)
	for root,dirs,files in os.walk(hh_dir):
		for file in files:
			if file.endswith(".txt"):
				if file not in idx_str:
					f = os.path.join(root, file)
					file_list.append( str(f) + os.linesep.strip('\n') )
				#mm.seek(0)
	return file_list

		#except: 
			#pass
		#with con:
		#	cur = con.cursor()
		#	cur.execute("CREATE TABLE IF NOT EXISTS Winnings( HandID TEXT, PlayerName TEXT, Net_Amount TEXT)")
		#	cur.executemany("INSERT INTO Winnings VALUES(?, ?, ?)", l)

			# put into list first?

			#mydbcursor.executemany(sql, (k + v for k, v in mydict.iteritems()))

if len(l) < 10000:
			cur.executemany( sql1, l)
			cur.executemany( sql2, l)
			cur.executemany( sql3, p)
		
		#else:
			sql1= ','.join(cur.mogrify('''(%(hand number)s, %(YY)s, %(MM)s, %(DD)s, %(max buyin)s, \
											%(min buyin)s, %(limit type)s, %(game type)s)''', x) for x in l)
			sql2 = ','.join(cur.mogrify('''( %(hand number)s, %(preflop)s, %(flop)s, %(turn)s, %(river)s)'''
											, x )for x in l)
			cur.execute('''INSERT INTO Hand_Info ( Hand_ID, YY , MM , DD , Max_Buyin , Min_Buyin , Limit_Type,\
						 Game_Type ) VALUES''' + sql1)
			cur.execute('''INSERT INTO Hand_Actions ( Hand_ID, Preflop, Flop, Turn, River ) VALUES''' + sql2) 





elif num == 1:
			ret = ''' SELECT player_name, yy, mm, dd, SUM( amount_won), count(*) FROM 
							(	SELECT * from hand_info AS HI INNER JOIN player_winnings 
								AS PW ON (HI.hand_id = PW.hand_id) ) 
						AS T GROUP BY player_name, yy, mm, dd ''' 
		elif num == 2:
			ret = ''' SELECT player_name, yy, mm, dd,sum( amount_won) FROM 
						(		SELECT * FROM hand_info AS HI INNER JOIN player_winnings 
								AS PW ON (HI.hand_id = PW.hand_id) ) 
						AS T WHERE player_name = %s GROUP BY player_name, yy, mm, dd''' % player

elif num == -1444:
			ret = '''SELECT 	
						date_trunc('hour', to_timestamp(dt) at time zone 'UTC'), 
						limit_type || game_type || ' ' || small_blind || '/' || big_blind, 
						count(*), sum(amount_won)
					FROM ( SELECT * from hand_info AS HI INNER JOIN player_winnings AS 
						PW ON (HI.hand_id = PW.hand_id) ) AS T WHERE UPPER(player_name) = UPPER('%s') 
					GROUP BY 
						game_type, limit_type, small_blind, big_blind,
						date_trunc('hour', to_timestamp(dt) at time zone 'UTC') ''' % player

import os
import re
import sys
import re
from regex_config import *

class SWC_PARSE:

	# Parses hand history and returns tuples indexing lines of valid hands
	def parseHH(self, hh):
		hand_idx = 	[]
		nn_idx 	 = 	[0] + \
					[l.start() for l in re.finditer('\n\n', hh)] + \
					[len(hh)]

		for i in range( len(nn_idx) - 1 ):
			start 	= nn_idx[i]
			end 	= nn_idx[i+1]
			p_hand 	= hh[start:end]
			h_idx 	= self.__hasHand(p_hand)

			if (h_idx):
				hand_idx.append([start+h_idx[0], start+h_idx[1]])
		return hand_idx

	# Parses hand to determine if valid. Returns dictionary hand fields
	def parseHand(self, hand):
		hand_dict = {
				'info' 		: [m.groupdict() for m in RGX_CONF_info.finditer(hand)],
				'players' 	: [m.groupdict() for m in RGX_CONF_players.finditer(hand)],
				'dealer'	: [m.groupdict() for m in RGX_CONF_dealer.finditer(hand)],
				'sb'		: [m.groupdict() for m in RGX_CONF_sb.finditer(hand)],
				'bb'		: [m.groupdict() for m in RGX_CONF_bb.finditer(hand)],
				'sbbb'		: [m.groupdict() for m in RGX_CONF_sbbb.finditer(hand)],
				'winners'	: [m.groupdict() for m in RGX_CONF_winner.finditer(hand)],
				'hero'		: [m.groupdict() for m in RGX_CONF_hero.finditer(hand)],
				'refund'	: [m.groupdict() for m in RGX_CONF_refund.finditer(hand)],
				'preflop' 	: RGX_CONF_preflop.search(hand),
				'flop'		: RGX_CONF_flop.search(hand),
				'turn'		: RGX_CONF_turn.search(hand),
				'river'		: RGX_CONF_river.search(hand),
				'showdown' 	: RGX_CONF_sd.search(hand),
				'rake'		: RGX_CONF_rake.search(hand) }

		if ( self.__isValidHand(hand_dict) ):
			#preflop actions
			if (hand_dict['flop']):
				h = hand[ hand_dict['preflop'].end():hand_dict['flop'].start() ]
				hand_dict['actions preflop']  = [m.groupdict() for m in RGX_CONF_action.finditer(h)]
				hand_dict['cards flop'] = hand_dict['flop'].groups()[0]
			else:
				h = hand[ hand_dict['preflop'].end():hand_dict['rake'].start() ]
				hand_dict['actions preflop'] = [m.groupdict() for m in RGX_CONF_action.finditer(h)]
				hand_dict['cards flop'] = ''
			#flop actions
			if (hand_dict['turn']):
				h = hand[ hand_dict['flop'].end():hand_dict['turn'].start() ]
				hand_dict['actions flop']  = [m.groupdict() for m in RGX_CONF_action.finditer(h)]
				hand_dict['cards turn'] = hand_dict['turn'].groups()[0]
			else:
				h = hand[ hand_dict['flop'].end():hand_dict['rake'].start() ]
				hand_dict['actions flop'] = [m.groupdict() for m in RGX_CONF_action.finditer(h)]
				hand_dict['cards turn'] = ''
			#turn actions
			if (hand_dict['river']):
				h = hand[ hand_dict['turn'].end():hand_dict['river'].start() ]
				hand_dict['actions turn']  = [m.groupdict() for m in RGX_CONF_action.finditer(h)]
				hand_dict['cards river'] = hand_dict['river'].groups()[0]
			else:
				h = hand[ hand_dict['turn'].end():hand_dict['rake'].start() ]
				hand_dict['actions turn'] = [m.groupdict() for m in RGX_CONF_action.finditer(h)]
				hand_dict['cards river'] = ''
			#river actions
			if (hand_dict['river']):
				h = hand[ hand_dict['river'].end():hand_dict['rake'].start() ]
				hand_dict['actions river'] = [m.groupdict() for m in RGX_CONF_action.finditer(h)]
				hand_dict['cards showdown'] = hand_dict['showdown'].groups()[0]
			else:
				hand_dict['actions river'] = []
				hand_dict['cards showdown'] = ''
			# shown hands
			if (hand_dict['cards showdown']):
				hand_dict['cards shown'] = [m.groupdict() for m in RGX_CONF_shown.finditer(h)]
			else:
				hand_dict['cards shown'] = ''

			hand_dict['rake paid'] = hand_dict['rake'].groups()[0]

			try:
				del hand_dict['preflop']
				del hand_dict['rake']
				del hand_dict['showdown']
			except KeyError:
				pass
		else:
			return False
		return hand_dict

	# sets additional attributes based on fields in hand dictionary
	def setAttr(self, d):
		players 	= [ p for i in range(len(d['players'])) \
						for p in [d['players'][i]['player']] ]
		net 		= {p: 0 for p in players}

		# bets lost as sb
		sb_bets = self.__flattenBets(d['sb'])
		net = self.__accumulateBets(net, sb_bets)
		# bets lost as bb
		bb_bets = self.__flattenBets(d['bb'])
		net = self.__accumulateBets(net, bb_bets)
		# bets lost posting sb & bb
		if (d['sbbb']):
			sbbb_bets = self.__flattenBets(d['sbbb'])
			net = self.__accumulateBets(net, sbbb_bets)
		# bets lost preflop
		pre_bets = self.__flattenBets(d['actions preflop'])
		net = self.__accumulateBets(net, pre_bets)
		# bets lost on flop
		if (d['flop']):
			flop_bets = self.__flattenBets(d['actions flop'])
			net = self.__accumulateBets(net, flop_bets)
		# bets lost on turn
		if (d['turn']):
			turn_bets = self.__flattenBets(d['actions turn'])
			net = self.__accumulateBets(net, turn_bets)
		# bets lost on river
		if (d['river']):
			river_bets = self.__flattenBets(d['actions river'])
			net = self.__accumulateBets(net, river_bets)
		#bets refunded
		if (d['refund']):
			refunds = self.__flattenBets(d['refund'])
			net = self.__accumulateBets(net, refunds)
		#bets won
		winners = self.__flattenBets(d['winners'])
		net = self.__accumulateBets(net, winners, 1)
		return net

	# checks segment of HH for existence of a hand
	def __hasHand(self, str):
		h = RGX_CONF_hand.search(str)
		r = RGX_CONF_rake.search(str)
		if (h and r):
			return [h.start(), r.end()]
		else:
			return []

	# checks segments of hand to determine if valid
	def __isValidHand(self, d):
		valid_setup = 	bool(d['info'] and \
							d['players'] and \
							d['dealer'] and \
							d['sb'] and \
							d['bb'] and \
							d['preflop'] and \
							d['rake'] and\
							d['winners'])

		valid_segments = True
		if (d['showdown']):
			if (d['flop'] and d['turn'] and d['river']):
				valid_segments = True
			else:
				valid_segments = False
		elif (d['river']):
			if (d['flop'] and d['turn']):
				valid_segments = True
			else:
				valid_segments = False
		elif (d['turn']):
			if (d['flop']):
				valid_segments = True
			else:
				valid_segments = False
		return bool(valid_segments and valid_setup)

	# flattens list of dictionaries with chip counts
	def __flattenBets(self, d):
		ret = dict()
		for i in range(len(d)):
			p = d[i]['player']
			c = float( 0 if d[i]['chips'] is None else d[i]['chips'])
			if ( p in ret.keys() ):
				ret[p] = ret[p] + c
			else:
				ret[p] = c
		return ret
		
	# adds two dictionaries of floating point values
	def __accumulateBets(self, a, d, additive = -1):
		acc_dict = a.copy()
		for player in d.keys():
			if player in acc_dict.keys():
					acc_dict[player] = 	acc_dict[player] + \
										additive * float(0 if d[player] is None else d[player])
			else:
				acc_dict[player] = additive * float(0 if d[player] is None else d[player])
		return acc_dict






#!/usr/bin/python2.7

from swc_api import SWC_API
import time
import thread
import re
from regex_config import *





def _updateTables(stream, active_list= set()):
	for s in stream:
		if( 'RingGameLobby' in s['Command']):
			table_count = int(s['Count'][0])
			# add to active tables if players at table is > 1, deactivate tables if in active list and players < 2
			active = set([s['ID%s'%i][0] for i in range(1, table_count+1) \
				for k in s['Players%s'%i] if int(k) > 1 ])
			deactive = set([s['ID%s'%i][0] for i in range(1, table_count+1) \
				for k in s['Players%s'%i] if (int(k) < 2  and s['ID%s'%i][0] in active_list) ])
			ret = (set(active_list) | active) - deactive
		else:
			ret = set(active_list)
	return ret

def _updateLogins(stream, login_list = set()):
	#may need to sort keys first...worry about this later.
	# Assuming the list has one logins event per user...
	#--> can do checks with stream['Total'] == len(login_list)
	for s in stream:
		if( 'Logins' in s['Command']):
			logins = [ [i,k[:pos]] for i,j in s.iteritems() \
				for k in j \
				for pos in [k.find('|')] \
				if i[:2] in ['LI', 'LO'] ]
			log_in = set([name for l in logins for name in [l[1]] if l[0][:2] == 'LI'] )
			log_out = set([name for l in logins for name in [l[1]] if l[0][:2] == 'LO'] )
			ret = (login_list | log_in ) - log_out
		else:
			ret = set(login_list)
		return ret

def logChat(stream, logname):
	for s in stream:
		if( 'LobbyChat' in s['Command']):
			text = "%s: %s %s" % (s['Player'][0], s['Text'][0], s['Color'])
			with open(logname, 'a') as f:
				f.write(text)
			return True
		else:
			return False

def logHH(stream, lines):
	whitelist = [' ', '.', '#',':', '-']
	for s in stream:
		if 'History' in s['Command']: # move higher up
			try:
				text = s['Text'][0]
				name = s['Table'][0]
				date = re_date.findall(text) 
				if name not in lines.keys():
					lines[name] = ['',False, False, '']
				l = lines[name]
				if (name in NO_RAKE_TABLES):
					if (text.find(re_sub['hand']) != -1 and l[0].find(re_sub['hand']) != -1): #Delay writing to catch split pots properly
						filename = "".join(x for x in ('HH%s %s'%(l[3], name)) if x.isalnum() or x in whitelist) + '.txt'
						with open(filename,'a') as f:
							f.write(l[0])
						l[0:] = ['',False, False, '']
				if (date):
					hh_buffer = '\n\n%s' % text
					suffix = date[0] #date stamp
					l[0:] = [ hh_buffer, True, False, suffix]
				elif (text.find(re_sub['rake']) != -1):
					hh_buffer = '%s\n%s' % (l[0], text)
					l[0:] = [hh_buffer, l[1], True, l[3]]
				else:
					hh_buffer = '%s\n%s' % (l[0], text)
					l[0:] = [hh_buffer, l[1], l[2], l[3]]
				if (l[1] == True and l[2] == True):
					filename = "".join(x for x in ('HH%s %s'%(l[3], name)) if x.isalnum() or x in whitelist) + '.txt'
					with open(filename,'a') as f:
						f.write(l[0])
					l[0:] = ['',False, False, '']
			except:
				pass

def logDiagnostics( mins):
	out = []
	T = SWC('Responsible', 'sealssuite')
	T.openConnection()
	timeout = time.time() + mins*60
	while time.time() < timeout:
		out = out + T.stream(False)
	T.closeConnection()
	return out

def main(mins):
	import datetime
	T = SWC_API('Responsible', 'sealssuite')
	T.openConnection()
	timeout = time.time() + mins*60
	#datetimestamp =  datetime.datetime.now().strftime("%Y%m%d")
	open_tables = []
	lines = {}
	active_tables = {}
	while time.time() < timeout:
		stream = T.stream(False)
		active_tables = _updateTables(stream, active_tables)
		to_open = active_tables - set(open_tables)
		to_close = set(open_tables) - active_tables
		for t in to_open:
			T.sendOpenTable('R', t)
			open_tables.append(t)
			print "open %s | %s" % (t, len(open_tables))
			lines[t] = ['', False, False]
		for t in to_close:
			T.sendCloseTable('R',t)
			open_tables.remove(t)
			lines[t] = ['', False, False]
			print "close %s | %s" % (t, len(open_tables))
		logHH(stream, lines)
	T.closeConnection()


main(820)







# Threading example
#def myfunction(string sleeptime lock *args):
#    while 1:
#        lock.acquire()
#        time.sleep(sleeptime)
#        lock.release()
#        time.sleep(sleeptime)

#if __name__ == "__main__":
#    lock = thread.allocate_lock()
#    thread.start_new_thread(myfunction ("Thread #: 1" 2 lock))
#    thread.start_new_thread(myfunction ("Thread #: 2" 2 lock))




	def __flattenBets(self, d):
		ret = dict()
		for i in range(len(d)):
			p = d[i]['player']
			c = float( 0 if d[i]['chips'] is None else d[i]['chips'])
			if ( p in ret.keys() ):
				ret[p] = ret[p] + c
			else:
				ret[p] = c
		return ret

	def __accumulateBets(self, a, d, additive = -1):
		acc_dict = a.copy()
		for player in d.keys():
			if player in acc_dict.keys():
					acc_dict[player] = 	acc_dict[player] + \
										additive * float(0 if d[player] is None else d[player])
			else:
				acc_dict[player] = additive * float(0 if d[player] is None else d[player])
		return acc_dict

		


turn_initiative = plyr

				# Update bet_states						 
				if (bet_state_prog['preflop'] == []):
					bet_state_prog['preflop'] =  bet_state[2]
				else:
					s_idx = bet_state.index(bet_state_prog['preflop'][-1])
					s_idx = ( 	(s_idx + 2) if s_idx == 0 \
								else (s_idx + 1) if s_idx <= len(bet_state)  \
								else s_idx )
					bet_state_prog['preflop'].append( bet_state[s_idx] )



		# turn stats
		for a in d['actions turn']:
			plyr = a['player']
			stats_p = stats[plyr]


			if a['action'] == 'folds':
				if plyr == turn_initiative:
					if (bet_state_prog['turn']):
						if bet_state_prog['turn'][-1] == bet_state[0]:
							stats_p['opp cbet'] = 1
					else:
						stats_p['opp cbet'] = 1

				# Update bet_states	
				if (bet_state_prog['turn'] == []):
					bet_state_prog['turn'] =  bet_state[0]
				else:
					bet_state_prog['turn'].append( bet_state_prog['turn'][-1] )

			elif a['action'] == 'checks':			
				if plyr == turn_initiative:
					if (bet_state_prog['turn']):
						if bet_state_prog['turn'][-1] == bet_state[0]:
							stats_p['opp cbet'] = 1
					else:
						stats_p['opp cbet'] = 1

				# Update bet_states	
				if (bet_state_prog['turn'] == []):
					bet_state_prog['turn'] =  bet_state[0]
				else:
					bet_state_prog['turn'].append( bet_state_prog['turn'][-1] )

			elif a['action'] == 'calls':

				# Update bet_states	
				bet_state_prog['turn'].append( bet_state_prog['turn'][-1] )

			elif a['action'] == 'bets':
				river_initiative = plyr

				#CBET
				if plyr == turn_initiative:
					stats_p['opp cbet'] = 1
					stats_p['cbet'] = 1

				# Update bet_states						 
				bet_state_prog['turn'].append( bet_state[1] )
			
			if a['action'] == 'raises':
				river_initiative = plyr


				# Update bet_states						 
				if (bet_state_prog['turn'] == []):
					bet_state_prog['turn'] =  bet_state[2]
				else:
					s_idx = bet_state.index(bet_state_prog['turn'][-1])
					s_idx = ( 	(s_idx + 2) if s_idx == 0 \
								else (s_idx + 1) if s_idx <= len(bet_state)  \
								else s_idx )
					bet_state_prog['turn'].append( bet_state[s_idx + 1] )


				stats_p['bet turn'] = 	-float(0 if a['chips'] is None else a['chips'])
			else:
				stats_p['bet turn'] = 	stats_p['bet turn'] - \
										float(0 if a['chips'] is None else a['chips'])



if (a['action'] in ['folds', 'checks', 'calls']):
	# Update bet_states	
	if (bet_state_prog['preflop'] == []):
		bet_state_prog['preflop'] =  bet_state[0]
	else:
		bet_state_prog['preflop'].append( bet_state_prog['preflop'][-1] )

elif (a['action'] == 'raises'): 
	if (bet_state_prog['preflop']):
		s_idx = bet_state.index(bet_state_prog['preflop'][-1])
					s_idx = ( 	(s_idx + 2) if s_idx == 0 \
								else (s_idx + 1) if s_idx <= len(bet_state)  \
								else s_idx )
					bet_state_prog['preflop'].append( bet_state[s_idx] )
	else:
		bet_state_prog['preflop'] =  bet_state[2]






if (a['action'] in ['folds', 'checks', 'calls']):
	if (bet_state_prog['preflop']):	
		if bet_state_prog['preflop'][-1] == bet_state[0]:
			stats_p['opp pfr'] = 1
	else:
		stats_p['opp pfr'] = 1

elif(a['action'] == 'raises'):




if (bet_state_prog['preflop']):
	if (a['action'] in ['folds', 'checks']):
		if bet_state_prog['preflop'][-1] == bet_state[0]:
			stats_p['opp pfr'] = 1
	elif (a['action'] == 'calls'):
		stats_p['vpip'] = 1
		if bet_state_prog['preflop'][-1] == bet_state[0]:
			stats_p['opp pfr'] = 1
	elif(a['action'] == 'raises'):
		stats_p['vpip'] = 1
		flop_initiative = plyr
		if bet_state_prog['preflop'][-1] == bet_state[0]:
			stats_p['opp pfr'] = 1
		#if bet_state is limped then this player pfr
			stats_p['pfr'] = 1
		#if bet_state is raised then this player 3bet
		if bet_state_prog['preflop'][-1] == bet_state[2]:
			stats_p['3bet preflop'] = 1
		#if bet_state is 3bet then this player 4bet
		if bet_state_prog['preflop'][-1] == bet_state[3]:
			stats_p['4bet preflop'] = 1
		#if bet_state is 4bet then this player >4bet
		if bet_state_prog['preflop'][-1] == bet_state[4]:
			stats_p['>4bet preflop'] = 1
		#if bet_state is >4bet then this player >4bet
		if bet_state_prog['preflop'][-1] == bet_state[5]:
			stats_p['>4bet preflop'] = 1
else:
	stats_p['opp pfr'] = 1
	if (a['action'] == 'raises'):
		stats_p['pfr'] = 1







# Update Stats Based on Previous bet_state
			if (a['action'] in ['folds', 'checks']):				
				if (bet_state_prog['preflop']):
					if bet_state_prog['preflop'][-1] == bet_state[0]:
						stats_p['opp pfr'] = 1
				else:
					stats_p['opp pfr'] = 1
					# if bet_state is 3bet then this player folds to 3bets
			elif (a['action'] == 'calls'):
				stats_p['vpip'] = 1
				if (bet_state_prog['preflop']):
					if bet_state_prog['preflop'][-1] == bet_state[0]:
						stats_p['opp pfr'] = 1
					# if bet_state is 3bet then this player called a 3bet
			if (a['action'] == 'raises'):
				stats_p['vpip'] = 1
				flop_initiative = plyr
				if (bet_state_prog['preflop']):					
					if bet_state_prog['preflop'][-1] == bet_state[0]:
						stats_p['opp pfr'] = 1
					#if bet_state is limped then this player pfr
						stats_p['pfr'] = 1
					#if bet_state is raised then this player 3bet
					if bet_state_prog['preflop'][-1] == bet_state[2]:
						stats_p['3bet preflop'] = 1
					#if bet_state is 3bet then this player 4bet
					if bet_state_prog['preflop'][-1] == bet_state[3]:
						stats_p['4bet preflop'] = 1
					#if bet_state is 4bet then this player >4bet
					if bet_state_prog['preflop'][-1] == bet_state[4]:
						stats_p['>4bet preflop'] = 1
					#if bet_state is >4bet then this player >4bet
					if bet_state_prog['preflop'][-1] == bet_state[5]:
						stats_p['>4bet preflop'] = 1
				else:
					stats_p['opp pfr'] = 1
					stats_p['pfr'] = 1



					if (a['action'] in ['folds', 'checks', 'calls']):
				if (bet_state_prog['preflop']):
					bet_state_prog['preflop'].append( bet_state_prog['preflop'][-1] )
				else:
					bet_state_prog['preflop'] =  bet_state[0]	
			elif (a['action'] == 'raises'): 
				if (bet_state_prog['preflop']):
					s_idx = bet_state.index(bet_state_prog['preflop'][-1])
					s_idx = ((s_idx + 2) if s_idx == 0 \
							else (s_idx + 1) if s_idx <= len(bet_state)  \
							else s_idx )
					bet_state_prog['preflop'].append( bet_state[s_idx] )
				else:
					bet_state_prog['preflop'] =  bet_state[2]


			if (bet_state_prog['preflop']):
				if (a['action'] in ['folds', 'checks', 'calls']):
					bet_state_prog['preflop'].append( bet_state_prog['preflop'][-1] )
				elif(a['action'] == 'raises'):
					s_idx = bet_state.index(bet_state_prog['preflop'][-1])
					s_idx = ((s_idx + 2) if s_idx == 0 \
							else (s_idx + 1) if s_idx <= len(bet_state)  \
							else s_idx )
					bet_state_prog['preflop'].append( bet_state[s_idx] )
			else:
				if (a['action'] in ['folds', 'checks', 'calls']):
					bet_state_prog['preflop'] =  bet_state[0]
				elif(a['action'] == 'raises'):
					bet_state_prog['preflop'] =  bet_state[2]






''
							'bet flop'			: 0, \
							'check flop'		: 0, \
							'opp bet flop'		: 0, \

							'call flop'			: 0, \
							'raise flop'		: 0, \
							'opp call flop' 	: 0, \


			if a['action'] == 'folds':
				if plyr == flop_initiative:
					if (bet_state_prog['flop']):
						if bet_state_prog['flop'][-1] == bet_state[0]:
							stats_p['opp cbet flop'] = 1
					else:
						stats_p['opp cbet flop'] = 1

				# Update bet_states	
				if (bet_state_prog['flop'] == []):
					bet_state_prog['flop'] =  bet_state[0]
				else:
					bet_state_prog['flop'].append( bet_state_prog['flop'][-1] )

			elif a['action'] == 'checks':			
				if plyr == flop_initiative:
					if (bet_state_prog['flop']):
						if bet_state_prog['flop'][-1] == bet_state[0]:
							stats_p['opp cbet flop'] = 1
					else:
						stats_p['opp cbet flop'] = 1

				# Update bet_states	
				if (bet_state_prog['flop'] == []):
					bet_state_prog['flop'] =  bet_state[0]
				else:
					bet_state_prog['flop'].append( bet_state_prog['flop'][-1] )

			elif a['action'] == 'calls':

				# Update bet_states	
				bet_state_prog['flop'].append( bet_state_prog['flop'][-1] )

			elif a['action'] == 'bets':
				turn_initiative = plyr

				#CBET
				if plyr == flop_initiative:
					stats_p['opp cbet flop'] = 1
					stats_p['cbet'] = 1

				# Update bet_states						 
				bet_state_prog['flop'].append( bet_state[1] )


			if a['action'] == 'raises':
				turn_initiative = plyr


				# Update bet_states						 
				if (bet_state_prog['flop'] == []):
					bet_state_prog['flop'] =  bet_state[2]
				else:
					s_idx = bet_state.index(bet_state_prog['flop'][-1])
					s_idx = ( 	(s_idx + 2) if s_idx == 0 \
								else (s_idx + 1) if s_idx <= len(bet_state)  \
								else s_idx )
					bet_state_prog['flop'].append( bet_state[s_idx + 1] )


				stats_p['bet flop'] = 	-float(0 if a['chips'] is None else a['chips'])
			else:
				stats_p['bet flop'] = 	stats_p['bet flop'] - \
										float(0 if a['chips'] is None else a['chips'])


####################################################


			# Update bet_states
			if (bet_state_prog['preflop']):
				if (a['action'] in ['folds', 'checks', 'calls']):
					bet_state_prog['preflop'].append( bet_state_prog['preflop'][-1] )
				elif(a['action'] == 'raises'):
					s_idx = bet_state.index(bet_state_prog['preflop'][-1])
					s_idx = ((s_idx + 2) if s_idx == 0 \
							else (s_idx + 1) if s_idx <= len(bet_state)  \
							else s_idx )
					bet_state_prog['preflop'].append( bet_state[s_idx] )
			else:
				if (a['action'] in ['folds', 'checks', 'calls']):
					bet_state_prog['preflop'] =  bet_state[0]
				elif(a['action'] == 'raises'):
					bet_state_prog['preflop'] =  bet_state[2]

			# Update Wagers Made
			if (a['action'] == 'raises'):
				stats_p['bet preflop'] = -float(0 if a['chips'] is None else a['chips'])
				stats_p['bet sb'] = 0
				stats_p['bet bb'] = 0
				stats_p['bet bb'] = 0
			else:
				stats_p['bet preflop'] = stats_p['bet preflop'] - \
										 float(0 if a['chips'] is None else a['chips'])


			#############################################################

		if a['action'] == 'folds':
				if plyr == turn_initiative:
					if (bet_state_prog['turn']):
						if bet_state_prog['turn'][-1] == bet_state[0]:
							stats_p['opp cbet'] = 1
					else:
						stats_p['opp cbet'] = 1

				# Update bet_states	
				if (bet_state_prog['turn'] == []):
					bet_state_prog['turn'] =  bet_state[0]
				else:
					bet_state_prog['turn'].append( bet_state_prog['turn'][-1] )

			elif a['action'] == 'checks':			
				if plyr == turn_initiative:
					if (bet_state_prog['turn']):
						if bet_state_prog['turn'][-1] == bet_state[0]:
							stats_p['opp cbet'] = 1
					else:
						stats_p['opp cbet'] = 1

				# Update bet_states	
				if (bet_state_prog['turn'] == []):
					bet_state_prog['turn'] =  bet_state[0]
				else:
					bet_state_prog['turn'].append( bet_state_prog['turn'][-1] )

			elif a['action'] == 'calls':

				# Update bet_states	
				bet_state_prog['turn'].append( bet_state_prog['turn'][-1] )

			elif a['action'] == 'bets':
				river_initiative = plyr

				#CBET
				if plyr == turn_initiative:
					stats_p['opp cbet'] = 1
					stats_p['cbet'] = 1

				# Update bet_states						 
				bet_state_prog['turn'].append( bet_state[1] )
			
			if a['action'] == 'raises':
				river_initiative = plyr


				# Update bet_states						 
				if (bet_state_prog['turn'] == []):
					bet_state_prog['turn'] =  bet_state[2]
				else:
					s_idx = bet_state.index(bet_state_prog['turn'][-1])
					s_idx = ( 	(s_idx + 2) if s_idx == 0 \
								else (s_idx + 1) if s_idx <= len(bet_state)  \
								else s_idx )
					bet_state_prog['turn'].append( bet_state[s_idx + 1] )


				stats_p['bet turn'] = 	-float(0 if a['chips'] is None else a['chips'])
			else:
				stats_p['bet turn'] = 	stats_p['bet turn'] - \
										float(0 if a['chips'] is None else a['chips'])




	# Update Stats Based on Previous bet_state		


							'vpip'
							'opp vpip'

							'call pf'
							'raise pf'
							'opp pfr'

							'call pfr'
							'3bet pf'
							'opp 3bet pf'

							'call 3bet'
							'4bet pf'
							'opp 4bet pf'

							'opp bet flop'
							'opp call flop'
							'check flop'
							'call flop'
							'bet flop'
							'raise flop'

							'check turn'
							'opp bet turn'
							'opp call turn'
							'call turn'
							'bet turn'
							'raise turn'

							'check river'
							'opp bet river'
							'opp call river'
							'call river'
							'bet river'
							'raise river'

							'wtsd'
							'w$sd'


							'opp bet flop'		: 0, \
							'opp call flop'		: 0, \
							'check flop'		: 0, \
							'call flop'			: 0, \
							'bet flop'			: 0, \
							'raise flop'		: 0, \
							'fold flop'			
'''
	
			# Update bet_states
			if (bet_state_prog[street] == []):
				if (a['action'] in ['folds', 'checks', 'calls']):
					bet_state_prog['preflop'].append(bet_state[1])
				elif(a['action'] == 'raises'):

			elif (bet_state_prog['preflop']):
				if (a['action'] in ['folds', 'checks', 'calls']):
					bet_state_prog[street].append( bet_state_prog['preflop'][-1] )
				elif(a['action'] == 'raises'):
					s_idx = bet_state.index(bet_state_prog['preflop'][-1])
					s_idx = ((s_idx + 2) if s_idx == 0 \
							else (s_idx + 1) if s_idx <= len(bet_state)  \
							else s_idx )
					bet_state_prog['preflop'].append( bet_state[s_idx] )

				

#---December 27th ---#
Important SQL shit:

	select HI.hand_id,HI.yy, HI.mm, HI.dd, PW.player_name, PW.amount_won from hand_info as HI inner join player_winnings as PW ON (HI.hand_id = PW.hand_id) limit 5;
	
	Select distinct limit_type, game_type, min_buyin, max_buyin FROM (select * from hand_info as HI inner join player_winnings as PW ON (HI.hand_id = PW.hand_id) ) as T where UPPER(player_name) = UPPER('Leoprox');

	Select player_name, yy, mm, dd, limit_type, game_type, min_buyin, max_buyin, sum( amount_won), count(*) FROM (select * from hand_info as HI inner join player_winnings as PW ON (HI.hand_id = PW.hand_id) ) as T where player_name = 'Leoprox' GROUP BY player_name, yy, mm, dd, limit_type, game_type, min_buyin, max_buyin;

	Select player_name, yy, mm, dd,sum( amount_won), count(*) FROM (select * from hand_info as HI inner join player_winnings as PW ON (HI.hand_id = PW.hand_id) ) as T GROUP BY player_name, yy, mm, dd;

	select to_timestamp(1420571061) at time zone 'UTC';

	select date_part('day', to_timestamp(1417661634) at time zone 'UTC');  

	SELECT 	
	date_trunc('hour', to_timestamp(dt) at time zone 'UTC'), 
	limit_type || game_type || ' ' || small_blind || '/' || big_blind, 
	count(*), sum(amount_won)
	FROM ( SELECT * from hand_info AS HI INNER JOIN player_winnings AS PW ON (HI.hand_id = PW.hand_id) ) AS T WHERE UPPER(player_name) = UPPER('Jdot') 
	GROUP BY 
	game_type, limit_type, small_blind, big_blind,
	date_trunc('hour', to_timestamp(dt) at time zone 'UTC');


	SELECT 	
	player_name, date_trunc('hour', to_timestamp(dt) at time zone 'UTC'), 
	limit_type || game_type || ' ' || small_blind || '/' || big_blind, 
	count(*), sum(amount_won), 
	FROM ( SELECT * from hand_info AS HI INNER JOIN player_winnings AS PW ON (HI.hand_id = PW.hand_id) ) AS T  
	GROUP BY 
	player_name, game_type, limit_type, small_blind, big_blind,
	date_trunc('hour', to_timestamp(dt) at time zone 'UTC');









#----------------
    <!--script type="text/javascript">

        $(function() {

            {% if net %}
                var data = [{{ net|safe }}];
            {% else %}
                var data = [];
            {% endif %}

            var options =   {
                                xaxes: [ { mode: "time" } ],
                                series: { lines:{ show: true, fill: true, lineWidth: 1.5, steps: true}},

                            }

            function doPlot() {
                $.plot("#placeholder", data, options);
            }
            doPlot();
            $(window).resize(function() {doPlot();});
    });

    </script-->







{% for table in tables %}
	    <div class="panel panel-mod">
	        <div class="panel-heading">
	            <h4 class="panel-title">
	                <a data-toggle="collapse" data-parent="#winnings" href="#collapse{{ table.limit_type }}">Collapsible Group Item #1</a>
	            </h4>
	        </div>
	        <div id="collapse{{ table.limit_type }}" class="panel-collapse collapse in">
	            <div class="panel-body">
	               {{ table }}
	            </div>
	        </div>
	    </div>
{% endfor %}

	    <div class="panel panel-mod">
	        <div class="panel-heading">
	            <h4 class="panel-title">
	                <a data-toggle="collapse" data-parent="#winnings" href="#collapseTwo">Collapsible Group Item #2</a>
	            </h4>
	        </div>
	        <div id="collapseTwo" class="panel-collapse collapse">
	            <div class="panel-body">
	                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	            </div>
	        </div>
	    </div>


	    <div class="panel panel-mod">
	        <div class="panel-heading">
	            <h4 class="panel-title">
	                <a data-toggle="collapse" data-parent="#winnings" href="#collapseThree">Collapsible Group Item #3</a>
	            </h4>
	        </div>
	        <div id="collapseThree" class="panel-collapse collapse">
	            <div class="panel-body">
	                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	            </div>
	        </div>
	    </div>



                        <div class="panel panel-mod">
                            <div class="panel-heading">
                                {{tables}}
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {% for gametable in table.games %}
                                            <tr>
                                                <td>1</td>
                                                <td>Mark</td>
                                                <td>Otto</td>
                                                <td>@mdo</td>
                                            </tr>
                                        {% endfor %}
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>


<body>


    <p>
      <input type=checkbox id="0" onClick="change(this)">
      <label for="0"> Chips</label><br/>
      <input type=checkbox id="1" checked onClick="change(this)">
      <label for="1"> Hands</label><br/>
    </p>

    <p>g.visibility() = <span id="visibility"></span></p>


    <script type="text/javascript">
      g = new Dygraph(
            document.getElementById("graph"), data,
        		{   legend: 'always',
                    labels: [ 'Date', 'Chips', 'Hands' ],
                    series: {'Chips': { axis: 'y' }, 'Hands': { axis: 'y2' },},axes: {y: {labelsKMB: true,independentTicks: true},y2: {labelsKMB: true,independentTicks: true}},
                    ylabel: 'Chip Won',
                    y2label: 'Hands Played',
                    yAxisLabelWidth: 60,
                    fillGraph: true,
                    labelsDivStyles: {
                    'text-align': 'right',
                    'background': 'none'
                  }
                  }

          );
      setStatus();

      function setStatus() {
        document.getElementById("visibility").innerHTML =
          g.visibility().toString();
      }

      function change(el) {
        g.setVisibility(parseInt(el.id), el.checked);
        setStatus();
      }
    </script>

  </body>


     g = new Dygraph( document.getElementById("graph"), data,
                 {      legend: 'always',
                        labels: [ 'Date', 'Chips', 'Hands' ],
                        series: {'Chips': { axis: 'y' }, 'Hands': { axis: 'y2' },},axes: {y: {labelsKMB: true,independentTicks: true},y2: {labelsKMB: true,independentTicks: true}},
                        ylabel: 'Chip Won',
                        y2label: 'Hands Played',
                        yAxisLabelWidth: 60,
                        fillGraph: true,
                        labelsDivStyles: {
                        'text-align': 'right',
                        'background': 'none'
                      }
                }
      );




						UPDATE Player_Summary SET 
											num_hands 	= old.num_hands + 1, 
											vpip 		= old.vpip 		+ %(vpip)s, 
											op_vpip 	= old.op_vpip 	+ %(op vpip)s, 
											pfr 		= old.pfr 		+ %(pfr)s,
											op_pfr 		= old.op_pfr 	+ %(op pfr)s,
											pf3b 		= old.pf3b 		+ %(pf3b)s,
											op_pf3b 	= old.op_pf3b 	+ %(op pf3b)s,
											f_pf3b 		= old.f_pf3b 	+ %(f pf3b)s,
											c_pf3b 		= old.c_pf3b 	+ %(c pf3b)s,
											r_pf3b 		= old.r_pf3b 	+ %(r pf3b)s,
											cbet 		= old.cbet 		+ %(cbet)s,
											op_cbet 	= old.op_cbet 	+ %(op cb)s,
											f_cbet 		= old.f_cbet 	+ %(f cb)s,
											c_cbet 		= old.c_cbet 	+ %(c cb)s,
											r_cbet 		= old.r_cbet 	+ %(r cb)s,
											Amount_Won 	= old.Amount_Won+ %(net)s,
											WTSD 		= old.WTSD 		+ %(wtsd)s,
											WSD 		= old.WSD 		+ %(wsd)s
										FROM (SELECT Apprx_dt, Player_Name, Small_Blind, Big_Blind, Limit_Type, Game_Type FROM Player_Summary 
													WHERE 
													Apprx_dt 	= date_trunc('hour', to_timestamp(%(date time)s) at time zone 'UTC') AND 
													Player_Name = %(player)s AND 
													Small_Blind = %(sb)s AND
													Big_Blind 	= %(bb)s AND
													Limit_Type 	= %(limit type)s AND
													Game_Type 	= %(game type)s ) AS old 
										WHERE 		Apprx_dt 	= date_trunc('hour', to_timestamp(%(date time)s) at time zone 'UTC') AND 
													Player_Name = %(player)s AND 
													Small_Blind = %(sb)s AND
													Big_Blind 	= %(bb)s AND
													Limit_Type 	= %(limit type)s AND
													Game_Type 	= %(game type)s AND
													NOT EXISTS ( SELECT hand_id FROM Hand_Info WHERE hand_id = %(h_id)s )


ret = '''SELECT 	
						date_trunc('hour', to_timestamp(dt) at time zone 'UTC'), 
						limit_type || game_type || ' ' || small_blind || '/' || big_blind, 
						count(*), sum(amount_won)
					FROM ( SELECT * from hand_info AS HI INNER JOIN player_winnings AS 
						PW ON (HI.hand_id = PW.hand_id) ) AS T WHERE UPPER(player_name) = UPPER('%s') 
					GROUP BY 
						game_type, limit_type, small_blind, big_blind,
						date_trunc('hour', to_timestamp(dt) at time zone 'UTC') ''' % player


SELECT Post.Id, temp.Count
FROM Post
LEFT JOIN
(SELECT Post.Id, COUNT(Comment.ID) AS Count
FROM Post
LEFT JOIN Comment ON Comment.PostId = Post.ID
LEFT JOIN Profile ON Profile.ID = Comment.ProfileID
WHERE Profile.Approved = 1
GROUP BY Post.Id)
temp ON temp.Id = Post.ID


CREATE TABLE Player_Summary AS
SELECT 	Count(*) AS Hand_Num,
		PW.player_name, 
		SUM(vpip) AS vpip, 
		SUM(op_vpip) AS op_vpip, 
		SUM(pfr) AS pfr, 
		SUM(op_pfr) AS op_pfr, 
		SUM(pf3b) AS pf3b, 
		SUM(op_pf3b) AS op_pf3b, 
		SUM(f_pf3b) AS f_pf3b, 
		SUM(c_pf3b) AS c_pf3b, 
		SUM(r_pf3b) AS r_pf3b, 
		SUM(cbet) AS cbet, 
		SUM(op_cbet) AS op_cbet, 
		SUM(f_cbet) AS f_cbet, 
		SUM(c_cbet) AS c_cbet, 
		SUM(r_cbet) AS r_cbet,

		SUM(Amount_Won) AS Amount_Won, 
		SUM(WTSD) AS WTSD,
		SUM(WSD) AS WSD,
		date_trunc('hour', to_timestamp(dt) at time zone 'UTC') AS Apprx_dt, 
		Small_Blind, 
		Big_Blind, 
		Limit_Type, 
		Game_Type

	FROM Player_Stats AS PS INNER JOIN Player_Winnings AS PW 
	ON ( PS.hand_id = PW.hand_id AND 
		PS.player_name = PW.player_name ) 
	INNER JOIN Hand_Info AS HI 
	ON ( HI.hand_id = PW.hand_id ) 
	GROUP BY 
		PW.player_name,
		game_type, 
		limit_type, 
		small_blind, 
		big_blind,
		date_trunc('hour', to_timestamp(dt) at time zone 'UTC')




select player_name, sum(amount_won) from player_summary where apprx_dt > (current_date - interval '1 day')
select player_name, sum(amount_won)  from player_summary where apprx_dt > (current_date - interval '1 week')
select player_name, sum(amount_won)  from player_summary where apprx_dt > (current_date - interval '1 month')
select player_name, sum(amount_won)  from player_summary where apprx_dt > (current_date - interval '1 year');

select player_name, sum(amount_won) as net from player_summary where apprx_dt > (current_date - interval '1 day') group by player_name order by net desc limit 10 UNION
select player_name, sum(amount_won) as net from player_summary where apprx_dt > (current_date - interval '1 day') group by player_name order by net asc limit 10;







<form class="input-group" id="selection_forms" method="post" action="{% url 'hud' %}" >

				<div class="col-lg-2">
	
			         	<label>Multiple Selects</label>
					        <select  multiple class="form-control" id="leftValues" size="4" >
					        	<option>Winnings </option>
					        	<option>Hands</option>
					            <option>Voluntarily Put chips In Pot (%)</option>
					            <option>Preflop Raise (%)</option>
					            <option>Preflop 3bet (%)</option>
					            <option>Fold to Preflop 3bet (%)</option>
					            <option>Call Preflop 3bet (%)</option>
					            <option>Preflop 4bet (%)</option>
					            <option>Continuation Bet Flop (%)</option>
					            <option>Fold to Flop Continuation Bet (%)</option>
					            <option>Call Flop Continuation Bet (%)</option>
					            <option>Raise Flop Continuation Bet (%)</option>
					            <option>Went to Showdown (%)</option>
					            <option>Won chips at Showdown (%)</option>

					        </select>
					    </section>
				</div>
				<div class="col-lg-1" >
				    <div class="form-group" >
				    	<br>
				    	<br>
				        <input type="button" id="btnLeft" value="&lt;&lt;" /><br>
				        <input type="button" id="btnRight" value="&gt;&gt;" />
				    </div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
				 		<label>Multiple Selects</label>
				        <select multiple class="form-control" id="rightValues" size="4">
				        	
				        </select>
				    </div>
		    	</div>
				{% csrf_token %}
			    <div class="col-lg-2">
				    <div class="form-group">    
						<label>Limit Type</label>
						<select class="form-control" name="limit_type" id="limit_type">
							<option value="All" selected>All</option>
							<option value="NL">No Limit</option>
							<option value="PL">Pot Limit</option>
							<option value="L">Limit</option>
						</select>
						
				    </div>
			    </div>

			    <div class="col-lg-2">
			    <div class="form-group">
					<label>Game Type</label>
					<select class="form-control" name="game_type" id="game_type">
						<option value="All" selected>All</option>
						<option value="HE">Holdem</option>
						<option value="O">Omaha</option>
						<option value="O8">Omaha Hi-Lo</option>
					</select>
			
			    </div>
			    </div>
			    <div class="col-lg-2">
			     <div class="form-group">
					<label>Blinds</label>
					<select class="form-control" name="blinds" id="blinds">
						<option value="All" selected>All</option>
						<option value="0.04">0.02/0.04</option>
						<option value="0.1">0.05/0.10</option>
						<option value="0.2">0.10/0.20</option>
						<option value="0.5">0.25/0.50</option>
						<option value="1">0.50/1</option>
						<option value="2">1/2</option>
						<option value="4">2/4</option>
						<option value="6">3/6</option>
						<option value="8">4/8</option>
						<option value="10">5/10</option>
						<option value="16">8/16</option>
						<option value="20">10/20</option>
						<option value="30">15/30</option>
						<option value="40">20/40</option>
						<option value="50">25/50</option>
						<option value="80">40/80</option>
						<option value="100">50/100</option>
						<option value="150">75/150</option>
						<option value="200">100/200</option>
						<option value="300">150/300</option>
						<option value="400">200/400</option>
						<option value="600">300/600</option>
					</select>

				</div>
				</div>
				</form>
				</div>



// mredkj.com tutorial007
// 2 techniques for submitting all options
// You only need to pick one of the techniques
//
// 2005-08-30 - created
// 2006-05-27 - updated
//
// setSubmitDebugOuput is only used in the client-side example
window.onload = setSubmitDebugOuput;
function setSubmitDebugOuput()
{
  document.getElementById('divOutput').innerHTML = window.location.search;
}
// Technique 1
function placeInHidden(delim, selStr, hidStr)
{
  var selObj = document.getElementById(selStr);
  var hideObj = document.getElementById(hidStr);
  hideObj.value = '';
  for (var i=0; i<selObj.options.length; i++) {
    hideObj.value = hideObj.value ==
      '' ? selObj.options[i].value : hideObj.value + delim + selObj.options[i].value;
  }
}
// Technique 2
function selectAllOptions(selStr)
{
  var selObj = document.getElementById(selStr);
  for (var i=0; i<selObj.options.length; i++) {
    selObj.options[i].selected = true;
  }
}

<form action="tutorial007.html" method="get" onsubmit="selectAllOptions('sel2');">





{
u'interval': [u'10 years'], u'game_type': [u'All'], u'submit': [u'Search'], 
u'stats_selected': [u'vpip', u'pfr', u'pf3b', u'cbet', u'f_cbet', u'amount_won', u'num_hands', u'f_pf3b', u'c_pf3b', u'r_pf3b', u'c_cbet', u'r_cbet', u'wtsd', u'wsd'], 
u'limit_type': [u'All'], u'query': [u'a'], u'csrfmiddlewaretoken': [u'1nj4j34JcdS7jUZ1XWnGqcmx56XQh00p'], u'blinds': [u'All']
}

	op_vpip 	= op_vpip 	+ %(op vpip)s, 
	pfr 		= pfr 		+ %(pfr)s,
	op_pfr 		= op_pfr 	+ %(op pfr)s,
	pf3b 		= pf3b 		+ %(pf3b)s,
	op_pf3b 	= op_pf3b 	+ %(op pf3b)s,
	f_pf3b 		= f_pf3b 	+ %(f pf3b)s,
	c_pf3b 		= c_pf3b 	+ %(c pf3b)s,
	r_pf3b 		= r_pf3b 	+ %(r pf3b)s,
	cbet 		= cbet 		+ %(cbet)s,
	op_cbet 	= op_cbet 	+ %(op cb)s,

	f_cbet 		= f_cbet 	+ %(f cb)s,
	c_cbet 		= c_cbet 	+ %(c cb)s,
	r_cbet 		= r_cbet 	+ %(r cb)s,


select 
	player_name, 
	sum(amount_won) as Amount_Won, 
	sum(num_hands) as Num_hands,
	sum(vpip)/(%(offset)s+sum(op_vpip)) as VPIP,
	sum(pfr)/(%(offset)s+sum(op_pfr)) as PFR,
	sum(pf3b)/(%(offset)s+sum(op_pf3b)) as PF3B,
	sum(cbet)/(%(offset)s+sum(op_cbet)) as CBET,
	(sum(f_pf3b) + sum(c_pf3b) + sum(r_pf3b)) as FacePF3B,
	sum(f_pf3b)/(%(offset)s+FacePF3B) as F_PF3B,
	sum(c_pf3b)/(%(offset)s+FacePF3B) as C_PF3B,
	sum(r_pf3b)/(%(offset)s+FacePF3B) as R_PF3B,
	(sum(f_cbet) + sum(c_cbet) + sum(r_cbet)) as FaceFCbet,
	sum(f_cbet)/(%(offset)s+FaceFCbet) as F_FCbet,
	sum(c_cbet)/(%(offset)s+FaceFCbet) as C_FCbet,
	sum(r_cbet)/(%(offset)s+FaceFCbet) as R_FCbet,
	sum(wtsd)/sum(num_hands) as WTSD,
	sum(wsd)/(%(offset)s+sum(wtsd)) as WSD
FROM player_summary WHERE
	player_name in %(pname)s AND
	apprx_dt > (current_date - interval %(time)s) AND 
	limit_type IN %(limit_type)s AND
	game_type IN %(game_type)s AND
	big_blind IN %(blinds)s
GROUP BY player_name limit %(limit)s;


qstr = '''select 
	player_name, 
	sum(amount_won) as Amount_Won, 
	sum(num_hands) as Num_hands,
	sum(vpip)/(0.01+sum(op_vpip)) as VPIP,
	sum(pfr)/(0.01+sum(op_pfr)) as PFR,
	sum(pf3b)/(0.01+sum(op_pf3b)) as PF3B,
	sum(cbet)/(0.01+sum(op_cbet)) as CBET,
	sum(f_pf3b)/(0.01+(sum(f_pf3b) + sum(c_pf3b) + sum(r_pf3b) )) as F_PF3B,
	sum(c_pf3b)/(0.01+(sum(f_pf3b) + sum(c_pf3b) + sum(r_pf3b) )) as C_PF3B,
	sum(r_pf3b)/(0.01+(sum(f_pf3b) + sum(c_pf3b) + sum(r_pf3b) )) as R_PF3B,
	sum(c_cbet)/(0.01+(sum(f_cbet) + sum(c_cbet) + sum(r_cbet) )) as C_FCbet,
	sum(f_cbet)/(0.01+(sum(f_cbet) + sum(c_cbet) + sum(r_cbet) )) as F_FCbet,
	sum(r_cbet)/(0.01+(sum(f_cbet) + sum(c_cbet) + sum(r_cbet) )) as R_FCbet,
	sum(wtsd)/sum(vpip) as WTSD,
	sum(wsd)/(0.01+sum(wtsd)) as WSD
FROM player_summary WHERE
	UPPER(player_name) in ('GERBERA', 'BLERPONE', 'THEWHO_') AND
	apprx_dt > (current_date - interval '1 month') AND 
	limit_type IN ('NL') AND
	game_type IN ('HE') AND
	big_blind IN (0.04, 0.1, 0.2, 0.5, 1, 2, 4, 6, 8, 10, 16, 20, 30, 40, 50, 80, 100, 150, 200, 300, 400, 600) 
GROUP BY player_name limit 50'''



player_name 
stats_map = {
	1 	:'Amount_Won',
	2 	:'Num_hands',
	3 	:'VPIP',
	4 	:'PFR',
	5 	:'PF3B',
	6 	:'CBET',
	7 	:'F_PF3B',
	8 	:'C_PF3B',
	9 	:'R_PF3B',
	10 	:'F_FCbet',
	11 	:'C_FCbet',
	12 	:'R_FCbet',
	13 	:'WTSD',
	14 	:'WSD' }




<div class="col-lg-11" style="padding-right:0%;padding-left:0%;margin-left: 5%;">



$.getJSON( "http://127.0.0.1:8000/menu", function( resp ) {
               // Keep a copy of the original json
               json = resp;

               // Put all returned objects in menuArr
               $.each(resp, function( key ) {
                   menuArr.push(resp[key]);
               });

               console.log(menuArr);

               // Check availabliity 
               if (menuArr[0] == "0" || menuArr[0] == "1") {
                   $(".loading").hide();
                   $("#errors").text("Nothing available right now");
               } else {
                   updateValues(i);
                   $(".loading").hide();
                   $("#item_card").show("slow");
                   $("#button_controls").show("slow");
               }
       });
it's the $.getJSON thing'



file = 'outdir/TH2015-01-28w.txt'
with open(file, 'r') as f:
	th = f.read()
from swc_db import *
S = SWC_DB('swc', 'postgres', 'postgres')
idx = S.parse.parseHistory(th, 'T')
for i in idx:
	tourney = th[ i[0]: i[1]]
	print tourney
	tdict = S.parse.parseTourney(tourney)
	print tdict



										'name' 			: tourney_info['name'],
										'end time'		: _epoch(tourney_info['end time'], '%Y-%m-%d %H:%M-%Z'),
										'buy in'		: tourney_info['buy in'],
										'num players'	: tourney_info['num players'],
										'player'		: p['player'],
										'net'			: net,
										'rank'			: p['rank'],
										'percentile'	: float(100/float(tourney_info['num players']))*(float(p['rank']) - 0.5), 
										'roi'			: '%.2f' % ( float(net)/float(tourney_info['buy in']) ),
										'itm'			: (1 if p['chips'] > 0 else 0)




# Ring Games
		z_r = [list(t) for t in zip(*ringgames)]
		_timestamp, _game, _hands, _won_chips = z_r[0], z_r[1], z_r[2], z_r[3]
		idx = sorted(range(len(_timestamp)), key = _timestamp.__getitem__)

		datetimestamp = []
		timestamp = []
		game = []
		hands = []
		won_chips = []
		won_bb = []
		cum_won_chips = [0]
		cum_won_bb = [0]
		cum_hands = [0]
		game_tables = {}

		dyg_all = 'Date,Chips,bb,Hands\\n'
		
		for i in idx:
			dt = str(_timestamp[i])
			datetimestamp.append( dt )
			game.append( _game[i] )
			hands.append( int(_hands[i]) )
			won_chips.append(  _won_chips[i] )
			game_bb = float( _game[i][ _game[i].find('/')+1:]  )
			won_bb.append(  float(_won_chips[i] )/game_bb  )

			cum_won_chips.append( cum_won_chips[-1] + won_chips[-1] )
			cum_won_bb.append( cum_won_bb[-1] + won_bb[-1] )
			cum_hands.append( cum_hands[-1] + hands[-1] )

			dyg_all 	= "%s%s,%s,%s,%s\\n" % ( dyg_all,
											datetimestamp[-1], 
											cum_won_chips[-1], 
											cum_won_bb[-1], 
											cum_hands[-1] )

			if game[-1] in game_tables.keys():
				_gt = game_tables[ game[-1] ]
				game_tables[ game[-1] ] = {	'game': game[-1], 
											'won_chips': _gt['won_chips'] + won_chips[-1] , 
											'won_bb': _gt['won_bb'] + won_bb[-1] , 
											'dt': datetimestamp[-1], 
											'hands': hands[-1]+_gt['hands'] }
			else:
				game_tables[ game[-1] ] = { 
											'game': game[-1], 
											'won_chips': won_chips[-1], 
											'won_bb': won_bb[-1], 
											'dt': datetimestamp[-1], 
											'hands': hands[-1] }
		tmp_tables = {}
		for t in game_tables.keys():
			limit_type = t[:t.find(' ')]
			if limit_type in tmp_tables.keys():
				_t = tmp_tables[ limit_type]
				tmp_tables[ limit_type ] = {'limit_type': limit_type, 
											'l_won': _t['l_won'] + game_tables[t]['won_chips'], 
											'l_hands': _t['l_hands'] + game_tables[t]['hands'],
											'games': _t['games'] + [game_tables[t]] }
			else:	
				tmp_tables[ limit_type ] = {'limit_type': limit_type, 
											'l_won': game_tables[t]['won_chips'], 
											'l_hands': game_tables[t]['hands'],
											'games': [game_tables[t]] }
		# Format hands and btc values
		for k in tmp_tables.keys():
			tmp_tables[k]['l_won'] = '{:20,.2f}'.format( tmp_tables[k]['l_won'] )
			tmp_tables[k]['l_hands'] = '{:20,.0f}'.format( tmp_tables[k]['l_hands'] )
			for j in tmp_tables[k]['games']:
				bb = float( j['game'][ j['game'].find('/')+1:]  )
				j['bb_100'] = '{:20,.2f}'.format(( j['won_chips']/bb ) * ( 100.0/j['hands'] ))
				j['won_chips'] 	= '{:20,.2f}'.format( j['won_chips'] )
				j['won_bb'] 	= '{:20,.2f}'.format( j['won_bb'] )
				j['hands'] 	= '{:20,.0f}'.format( j['hands'] )
				j['dt']		= datetime.strptime(j['dt'], "%Y-%m-%d %H:%M:%S").strftime("%A %d. %B %Y")

		tables = [tmp_tables[t] for t in tmp_tables.keys() ]



( Apprx_dt, Player_Name, Tournament_Name, Amount_Won, Buy_in, Num_Players, Rank, Percentile, ROI, ITM )

ret = '''Count(*), Tournament_Name, Apprx_dt, Amount_Won, Buy_in
ret = '''SELECT count(*), sum(amount_won), sum(amount_won)/(0.0001+sum(buy_in)) AS ROI, sum(ITM)/count(*) AS ITM, sum(Percentile)/count(*) AS Percentile
					
					Tournament_Name, Apprx_dt, Amount_Won, Buy_in

					count(*),
						sum(amount_won), 
						sum(amount_won)/(0.0001+sum(buy_in)) AS ROI,
						sum(ITM)/count(*) AS ITM,
						sum(Percentile)/count(*) AS Percentile




						SELECT
						count(*),
						buy_in,
						sum(amount_won), 
						sum(amount_won)/(0.0001+sum(buy_in)) AS ROI,
						sum(ITM)/count(*)::float AS ITMper,
						sum(Percentile)/count(*) AS Percentile
					FROM Tournament_Summary WHERE
						UPPER(player_name) = UPPER('jdot')
					GROUP BY player_name, Buy_in;

					2015-01-22 23:26:00


					Command=TournamentLobby&Clear=No&ID1=5%20Chip%20100%20GTD&Type1=T&Game1=NL%20Hold'em&Buyin1=4.75&EntryFee1=0.25&Rebuy1=&Shootout1=No&ShootTo1=0&TS1=9&Reg1=0&Max1=90&Starts1=Playing:%2024/28&StartMin1=0&StartTime1=2015-01-20%2020:30&Tables1=10&Password1=No&Count=1'
					Command=TournamentLobby&Clear=No&ID1=20%20Chip%20Deeper%20300%20GTD&Type1=T&Game1=NL%20Hold'em&Buyin1=19&EntryFee1=1&Rebuy1=&Shootout1=No&ShootTo1=0&TS1=9&Reg1=0&Max1=135&Starts1=Playing:%2015/20&StartMin1=0&StartTime1=2015-01-20%2020:00&Tables1=15&Password1=No&Count=1

'parse_tourney_name':
'parse_tourney_trunc':
'parse_tourney_date':
'parse_tourney_seats':
'parse_tourney_limit':
'parse_tourney_game':
'parse_tourney_buyin':
'parse_tourney_rake':
'parse_tourney_sng':
'parse_tourney_sch':
'parse_tourney_mtt':
'parse_tourney_gtd':
'parse_tourney_krill':
'parse_tourney_turbo':


'parse_tourney_trny_info': 
'parse_tourney_plyr_info':



r'^(?:Name\:\s(?P<name>[\s\w+\W+]+)\;)\n' + \
r'(?:Trunc\_name\:\s(?P<trunc>[\s\w+\W+]+)\;)\n' + \
r'(?:Date\:\s(?P<datetime>\d{4}\D\d{2}\D\d{2}\s\d{2}:\d{2})\;)\n' + \
r'(?:Seats\:\s(?P<seat>[0-9]+)\;)\n' + \
r'(?:Limit\:\s(?P<limit>NL|PL|L)\;)\n' + \
r'(?:Game\:\s(?P<game>O8|HE|O)\;)\n' + \
r'(?:Buyin\:\s(?P<buyin>[.0-9]+)\;)\n' + \
r'(?:Rake\:\s(?P<rake>[.0-9]+)\;)\n' + \
r'(?:SNG\:\s(?P<sng>True|False)\;)\n' + \
r'(?:Scheduled\:\s(?P<sch>True|False)\;)\n' + \
r'(?:MTT\:\s(?P<mtt>True|False)\;)\n' + \
r'(?:GTD\:\s(?P<gtd>True|False)\;)\n' + \
r'(?:Krill\:\s(?P<krill>True|False)\;)\n' + \
r'(?:Turbo\:\s(?P<turbo>True|False)\;)\n'


test = \
'''Name: 10 Chip PLO 100 GTD;
Trunc_name: 10 Chip PLO 100 GTD;
Date: 2015-01-28 08:46;
Seats: 2;
Limit: PL;
Game: O;
Buyin: 10.0;
Rake: 0.4;
SNG: False;
Scheduled: True;
MTT: True;
GTD: True;
Krill: False;
Turbo: False;
Player: CAAD1,1,19.60;DaScumbagKid,2,0;;
'''


Date: %(datetime)s;
Seats: %(seat type)s;
Limit: %(limit type)s;
Game: %(game type)s;
Buyin: %(buyin)s;
Rake: %(rake)s;
SNG: %(sng)s;
Scheduled: %(scheduled)s;
MTT: %(mtt)s;
GTD: %(gtd)s;
Krill: %(krill)s;
Turbo: %(turbo)s;
Player: %(positions)s;

'name' 			
'trunc' 		
'end time'		
'buy in'		
'rake' 			
'num players'	
'seats' 		
'limit type'	
'game type' 	
'sng' 			
'sch' 			
'mtt' 			
'gtd' 			
'krill'			
'turbo' 		

'player'
'net'			
'rank'			
'percentile'	
'roi'			
'itm'			

Amount_Won, Buy_in, Rake, Num_Players, Seats, Limit_Type, Game_Type, SNG, Scheduled, MTT, GTD, Krill, Turbo, Rank, Percentile FLOAT, ROI, ITM


SELECT name FROM (SELECT distinct trunc_tournament_name AS name, krill FROM tournament_summary WHERE scheduled = true order by krill, name ASC) AS T;

 select apprx_dt, tournament_name, buy_in, player_name, rank, amount_won from tournament_summary order by apprx_dt desc,tournament_name asc, rank asc

select tournament_name from tournament_summary where player_name='Micon' intersect select tournament_name from tournament_summary where player_name='gerbera';

select tournament_name from tournament_summary where player_name!='Micon' intersect select tournament_name from tournament_summary where player_name!='gerbera';




select tournament_name from tournament_summary where tournament_name in (select tournament_name from tournament_summary where player_name='Micon' union select tournament_name from tournament_summary where player_name='gerbera');



SELECT apprx_dt, tournament_name, buy_in, player_name, rank, amount_won 
	FROM tournament_summary WHERE date_trunc('day',apprx_dt) >= '2015-01-29' 
	AND date_trunc('day', apprx_dt) <= '2015-01-29' 

	ORDER BY apprx_dt DESC, tournament_name ASC, rank ASC;


	{u'tourney_name': u'All', u'end_date': u'2020-12-31', 
	
	'constructed query': u' AND buy_in = 1.0 AND limit_type = NL AND game_type = HE AND seats = 2 AND 0.04 = true', 
	u'game_type': u'HE', u'limit_type': u'NL', u'buyin': u'1.0', u'players': (), u'tourney_type': u'0.04', u'seat_type': u'2', 
	'query': u" \tSELECT apprx_dt, tournament_name, buy_in, player_name, rank, amount_won \n\t\t\t\t\t\tFROM tournament_summary \
	WHERE date_trunc('day',apprx_dt) >= 2014-12-01\n\t\t\t\t\t\tAND date_trunc('day', apprx_dt) <= 2020-12-31 AND buy_in = 1.0 AND limit_type = NL \
	AND game_type = HE AND seats = 2 AND 0.04 = true ORDER BY apprx_dt DESC, tournament_name ASC, rank ASC\n\t\t\t\t\t\t",



	select tournament_name from tournament_summary where tournament_name in 
	(select tournament_name from tournament_summary where player_name='Micon' AND apprx_dt  >= '2014-12-01' union select tournament_name from tournament_summary where player_name='gerbera') 
	AND tournament_name='Donkdown Freeroll';
<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                Tournaments
            </div>
            <!-- .panel-heading -->
            <div class="panel-body">
                <div class="panel-group" id="past_tournaments">
                    <div class="panel panel-default">

                       {% for tournament in tournament_info %}
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#past_tournaments" href="#collapse{{forloop.counter}}">
									<p style="text-align:left;margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;">
										<strong>{{tournament.name}}</strong>
										<span style="float:right;">
											Buyin: {{ tournament.buy_in }} Chips
										</span>
									</p>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{forloop.counter}}" class="panel-collapse collapse in">
                            <div class="panel-body">

	                            <div class="table-responsive">
		                            <table class="table table-striped table-hover">
		                                <thead>
		                                    <tr>
		                                    	<th style="text-align:center" >{{NAMES.rank}}</th>
		                                        <th style="text-align:center" >{{NAMES.player}}</th>
		                                        <th style="text-align:center" >{{NAMES.won}}</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                {% for p in tournament.tourney_info %}	
		                                    <tr>
		                                    	<td style="text-align:center">{{p.rank}}</td>
		                                        <td style="text-align:center">{{p.player}}</td>
		                                        <td style="text-align:center"> {{p.won}}</td>
		                                    </tr>
		                       			{% endfor %}
		                                </tbody>
		                            </table>
		                        </div>
                            
                            </div>
                        </div>
                     {% endfor %}
                    </div>
                </div>
            </div>
            <!-- .panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



{% if NAMES %}
<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                Tournaments
            </div>
            <!-- .panel-heading -->
            <div class="panel-body">
                <div class="panel-group" id="past_tournaments">
                    <div class="panel panel-default">

                       {% for tournament in tournament_info %}
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#past_tournaments" href="#collapseOne">
									<p style="text-align:left;margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;">
										<strong>{{tournament.name}}</strong>
										<span style="float:right;">
											Buyin: {{ tournament.buy_in }} Chips
										</span>
									</p>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">

	                            <div class="table-responsive">
		                            <table class="table table-striped table-hover">
		                                <thead>
		                                    <tr>
		                                    	<th style="text-align:center" >{{NAMES.rank}}</th>
		                                        <th style="text-align:center" >{{NAMES.player}}</th>
		                                        <th style="text-align:center" >{{NAMES.won}}</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                {% for p in tournament.tourney_info %}	
		                                    <tr>
		                                    	<td style="text-align:center">{{p.rank}}</td>
		                                        <td style="text-align:center">{{p.player}}</td>
		                                        <td style="text-align:center"> {{p.won}}</td>
		                                    </tr>
		                       			{% endfor %}
		                                </tbody>
		                            </table>
		                        </div>
                            
                            </div>
                        </div>
                     {% endfor %}
                    </div>
                </div>
            </div>
            <!-- .panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                        </li>



              if __name__ == '__main__':
	mins = 5000
	end_time = time() + mins*60
	while time() < end_time:
		ring = multiprocessing.Process(name='ring', target=stream_ring)
		ring.daemon = True
		tourney = multiprocessing.Process(target=stream_tourney)
		tourney.daemon = True
		tracker = multiprocessing.Process(name = 'tracker', target=trackFileChanges)
		tracker.daemon = False
		ring.start()
		tourney.start()
		tracker.start()
		tracker.join()
	#ring.terminate()



def stream_ring_process():
	while True:
		try:
			stream_manager(5000,['R'],0)
		except:
			sleep(15)
			print "Ring Process Exception"
			continue

def stream_tourney_process():
	while True:
		try:
			stream_manager(5000,['T'],1)
		except:
			sleep(15)
			print "Tournament Process Exception"
			continue

leaderboard_ring:
			#S.cur.execute(S.queryStr(10.1), qd)
			#_res = S.cur.fetchall()
			#_res = [ {'player': r[0], 'chips': '{:20,.2f}'.format( r[1] ) } for r in _res ]
			#ret[ 'lose_' + i.replace(' ','') ] = _res

			#S.cur.execute(S.queryStr(10.2), qd)
			#_res = S.cur.fetchall()
			#_res = [ {'player': r[0], 'chips': '{:20,.2f}'.format( r[1] ) } for r in _res ]
			#ret[ 'win_' + i.replace(' ','') ] = _res